# Per species DEG overlap between treatments
# Author: Maëlle Daunesse

# Loading library
library(dplyr)

overlap_data = data.frame()
for (species in c("Human", "Macaque", "Mouse")) {
  # Loading gene name information
    E2 = read.table(paste0("../../../results/RNA-seq/differential_analysis_DeApp/", species,"_E2_DeApp_signi/all3_overlap.txt"), header = T, sep = "\t") %>% pull(X)
    P4 = read.table(paste0("../../../results/RNA-seq/differential_analysis_DeApp/", species,"_P4_DeApp_signi/all3_overlap.txt"), header = T, sep = "\t") %>% pull(X)
    # Representation factor
    n = nrow(read.table(paste0("../../../results/RNA-seq/differential_analysis_DeApp/", species,"_E2_DeApp/all3_overlap.txt")))
    observed = length(E2[E2 %in% P4])
    expected = (length(E2)*length(P4))/n
    repfactor = observed/expected
    overlap_data = rbind(overlap_data, data.frame(species, E2 = length(E2), P4 = length(P4), n_gene = n, observed, expected, repfactor))
}